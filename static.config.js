import { request } from 'graphql-request'

const DRUPAL_GRAPHQL = 'http://18.196.251.29/graphql'

const drupal_query = `{ 
  nodeQuery {
      entities { 
          nid
          title
          body { 
              summary 
              value 
          } 
      }
  }
}`


export default {
  getRoutes: async () => {
    const {
      nodeQuery,
    } = await request(DRUPAL_GRAPHQL, drupal_query)

    const articles = nodeQuery.entities

    return [
      {
        path: '/',
        component: 'src/pages/Home',
        getData: () => ({
          articles,
        }),
        children: articles.map(article => ({
          path: `/articles/${article.nid}/`,
          component: 'src/pages/Article',
          getData: () => ({
            article,
          }),
        })),
      },
      {
        is404: true,
        component: 'src/pages/404',
      },
    ]
  },
}
