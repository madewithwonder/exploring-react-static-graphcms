import React from 'react'
import { withRouteData } from 'react-static'
import Markdown from 'react-markdown'

import { Tags } from '../components/Tags.js'

export default withRouteData(({ article }) => (
  <article>
    <h1 Style='font-size: 40px'>{article.title}</h1>
    <Tags />
    <Markdown
      source={article.body.value}
      escapeHtml={false}
    />
  </article>
))
