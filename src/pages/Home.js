import React from 'react'
import { withRouteData, Link } from 'react-static'

export default withRouteData(({ articles }) => (
  <section>
    <ul className="home-ul">
      {articles.map(article => (
        <li className="home-li" key={article.nid}>
          <Link to={`/articles/${article.nid}/`} className="home-link">
            <h3>{article.title}</h3>
            <p>{article.body.summary}</p>
          </Link>
        </li>
      ))}
    </ul>
  </section>
))
