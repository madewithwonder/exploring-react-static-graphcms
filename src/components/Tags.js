import React, { Component } from 'react'

import { request, GraphQLClient } from 'graphql-request'

const DRUPAL_GRAPHQL = 'http://18.196.251.29/graphql'

//
// This Tags component is only made to test if client side requests to the
// GraphQL server can be made at run-time
// Though Druapl GraphQL seems to expect slightly different structured calls
// from "standard" graphql services (like GraphCMS), the test succeeds
//

export class Tags extends Component {
  constructor () {
    super()

    this.state = {
      isLoaded: false,
      tags: [],
    }
  }

  componentDidMount () {

    const drupal_query = '{ nodeById(id:5) { nid, title} }'

    fetch(DRUPAL_GRAPHQL, {
      method: 'POST',
      mode: 'no-cors',
      body: `{ "query": "${drupal_query}"}`,
    }).then(response => { this.setState({ isLoaded: response }) })

  }

  render () {
    if (this.state.isLoaded) {
      return <small>Load graphQL data - success</small>
    }

    return <small>Load graphQL data - failed</small>
  }
}

